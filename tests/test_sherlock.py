import io

import pytest
from mock import patch 

from sherlock.sherlock import Sherlock
from tango import Database, DevFailed

from sherlock.utils.device import Device, Subdevice, ControllerFile

pool = 'pool_nice_pool'

db = {
    "controller/dummymotorcontroller/motor":{ 'properties': {'id': ['1'],'library': ["Random.py"]},
                                              'alias': 'motor',
                                              'class': 'Controller'},
    "controller/testcontroller/tc": {'properties': {'id': ['3'], 'source': ['sys/tg_test/1222'], 'library': ["Controller.py"]},
                                     'alias': 'tc',
                                     'class': 'Controller'},
    "dserver/Pool/nice": {'properties': {},
                          'class': 'DServer'},
    "expchan/tc/1": {'properties': {'id': ['4'], 'ctrl_id': ['3'], 'library': ["Dummytest.py"]},
                     'alias': 'tc1',
                     'class': 'TwoDExpChannel'},
    "expchan/tc/2": {'properties': {'id': ['5'], 'ctrl_id': ['3']},
                     'alias': 'tc2',
                     'class': 'TwoDExpChannel'},
    "mg/pool_nice_1/_mg_ms_10661_1": {'properties': {'id': ['8'], 'elements': ['2', '7']},
                                      'alias': 'mg',
                                      'class': 'MotorGroup'},
    "mntgrp/pool_nice_1/mg1": {'properties': {'id': ['6'], 'elements': ['4', '5', '11111']},
                               'alias': 'mg1',
                               'class': 'MeasurementGroup'},
    "motor/motor/1": {'properties': {'id': ['2'], 'ctrl_id': ['1'], 'Axis': ['1']},
                      'alias': 'motor1',
                      'class': 'Motor'},
    "motor/motor/2": {'properties': {'id': ['7'], 'ctrl_id': ['1'], 'Axis': ['2']},
                      'alias': 'motor2',
                      'class': 'Motor'},
    "Pool/nice/pool": {'properties': {'PoolPath': ['some/path']},
                      'alias': 'Pool_nice_pool',
                      'class': 'Pool'},
    f"MacroServer/{pool}": {'properties': {'MacroPath': ['some/path']},
                            'alias' : f"MacroServer_{pool}",
                            'class': 'MacroServer'
                            }

}

db_class = {
    f"Pool/{pool}" : {
        "Controller": ['controller/dummymotorcontroller/motor', 'controller/testcontroller/tc'],
        "DServer": ['dserver/Pool/nice'],
        "TwoDExpChannel": ['expchan/tc/1', 'expchan/tc/2'],
        "MotorGroup": ['mg/pool_nice_1/_mg_ms_10661_1'],
        "MeasurementGroup": ['mntgrp/pool_nice_1/mg1'],
        "Motor": ['motor/motor/1', 'motor/motor/2'],
        "Pool": ['Pool/nice/pool'],
        },
    f"MacroServer/{pool}": {
        'MacroServer': [f"MacroServer/{pool}"]
        }
}


def get_device_info(name):
    if name in ["sys/tg_test/1111", "zyla05"]:
        raise DevFailed
    else:
        return "Some info"

def get_alias_from_device(name):
    if name.isnumeric:
        device = list(filter(lambda x: x['class'] not in ['DServer', 'Pool','MacroServer'] and x['properties']['id'] == name, db.values()))
        name = device[0] if device else ""
    if name in ["sys/tg_test/1222", ""]:
        raise DevFailed([{'desc': 'description'}])
    else:
        return db[name]['alias']

def get_device_name(server, klass):
    if klass != '*':
        return db_class[server][klass]
    else:
        return [item for sublist in list(db_class[server].values()) for item in sublist]


def get_device_property(name, properties):
    properties = [properties] if not isinstance(properties, list) else properties
    props = {}
    for prop in properties:
        if prop in db[name]['properties'].keys():
            props[prop] = db[name]['properties'][prop]
        else:
            props[prop] = []
    return props

@pytest.yield_fixture()
def tango_db():
    with patch('sherlock.sherlock.Database') as db_mock:

        db_mock.return_value.get_device_name.side_effect = get_device_name
        db_mock.return_value.get_device_property.side_effect =  get_device_property
        db_mock.return_value.get_device_info.side_effect = get_device_info

        db_mock.return_value.get_class_for_device.side_effect = lambda name: db[name]['class']

        db_mock.return_value.get_alias_from_device.side_effect = get_alias_from_device

        db_mock.return_value.get_device_class_list.side_effect = lambda name: ['controller/dummymotorcontroller/motor', 'Controller',
                                                        'controller/testcontroller/tc', 'Controller',
                                                        'dserver/Pool/nice', 'DServer', 'expchan/tc/1',
                                                        'TwoDExpChannel', 'expchan/tc/2', 'TwoDExpChannel',
                                                        'mg/pool_nice_1/_mg_ms_10661_1', 'MotorGroup',
                                                        'mntgrp/pool_nice_1/mg1', 'MeasurementGroup',
                                                        'motor/motor/1', 'Motor', 'motor/motor/2', 'Motor',
                                                        'Pool/nice/pool', 'Pool', f"MacroServer/{pool}", 'MacroServer']

        yield db_mock


@pytest.yield_fixture()
def devfailed_mock():
    with patch('sherlock.sherlock.DevFailed.args') as df_mock:
        df_mock = "description"
        yield df_mock


def test_init(tango_db):
    sh = Sherlock("test-pool")

    assert sh.devices_in_pool == []
    assert sh.pool_name == "Pool/test-pool"


def test_get_devices_from_pool(tango_db):
    sh = Sherlock("test-pool")
    sh.get_devices_from_pool()

    assert len(sh.devices_in_pool) == len(db.keys()) - 2  # Except the pool instance in the DB
    assert isinstance(sh.devices_in_pool[1], Device)
    assert isinstance(sh.devices_in_pool[1].subdevices[0], Subdevice)
    assert isinstance(sh.devices_in_pool[1].file, ControllerFile)


def test_check_elements(tango_db, devfailed_mock):
    sh = Sherlock("test-pool")
    sh.get_devices_from_pool()

    d = Device(8, "mntgrp/pool_nice_1/mg1", "MeasurementGroup", "mg1",
               [Subdevice("1", "alias1"), Subdevice("9999", "alias2"), Subdevice("sys/tg_test/1111", ""), Subdevice("zyla05", "")])
    sh.check_subdevices(d)

    assert not d.subdevices[0].error
    assert d.subdevices[1].error
    assert d.subdevices[2].error
    assert d.subdevices[3].error


def test_get_files_from_pool(tango_db):
    sh = Sherlock("test-pool")

    sh.get_devices_from_pool()
    sh.get_files_from_pool()

    assert not sh.files_in_pool


def test_check_file(tango_db):
    sh = Sherlock("test-pool")

    sh.get_devices_from_pool()
    sh.files_in_pool = ["Controller.py"]

    sh.check_file(sh.devices_in_pool[0])
    sh.check_file(sh.devices_in_pool[1])

    assert sh.devices_in_pool[0].file.error
    assert not sh.devices_in_pool[1].file.error


def test_print_report(tango_db):
    sh = Sherlock("test-pool")

    from contextlib import redirect_stdout

    f = io.StringIO()
    with redirect_stdout(f):
        sh.start()

    tables = f.getvalue()

    assert "ERROR" in tables
    assert "111" in tables
