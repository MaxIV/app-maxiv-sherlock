from sherlock.utils.device import Device, Subdevice


def test_device_init():
    device = Device(1, "sys/tg_test/1", 'alias',"TangoTest", [1, "sys/tg_test/11", "zyla5"])

    assert device.id_ == 1
    assert device.name == "sys/tg_test/1"
    assert device.class_ == "TangoTest"
    assert device.subdevices == [1, "sys/tg_test/11", "zyla5"]


def test_subdevice_init():
    device = Subdevice(1, 'alias1', error=True)

    assert device.id_ == 1
    assert device.alias == 'alias1'
    assert device.error

    device = Subdevice(2, 'alias2', False)
    assert not device.error


def test_subdevice_str():
    device = Subdevice(2, 'alias2', False)
    assert str(device) == "Device 2. Error False"
