from sherlock.utils.colors import ColoredText


def test_color():
    assert ColoredText.color("WARNING") == "\033[33m" + "WARNING" + "\033[0m"
    assert ColoredText.color("ERROR") == "\033[31m" + "ERROR" + "\033[0m"
    assert ColoredText.color("OK") == "\033[32m" + "OK" + "\033[0m"
