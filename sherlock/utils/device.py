class Device:
    def __init__(self, id_, name, alias, class_, subdevices=None, file=None):
        self.id_ = id_
        self.name = name
        self.alias = alias
        self.class_ = class_

        self.subdevices = subdevices

        self.file = file


class Subdevice:
    def __init__(self, id_, alias, error=False, desc=''):
        self.id_ = id_
        self.alias = alias
        self.error = error
        self.desc = desc

    def __str__(self):
        return f"Device {self.id_}. Error {self.error}"


class ControllerFile:
    def __init__(self, name=None, error=False, desc=''):
        self.name = str(name[0]).strip() if name else None
        self.error = error
        self.desc = desc

    def __str__(self):
        return f"File {self.name}. Error {self.error}"
