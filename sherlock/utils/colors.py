class ColoredText:
    BLACK = '\033[30m'
    RED = '\033[31m'
    GREEN = '\033[32m'
    YELLOW = '\033[33m'
    BLUE = '\033[34m'
    MAGENTA = '\033[35m'
    CYAN = '\033[36m'
    WHITE = '\033[37m'
    GREY = '\33[90m'
    ENDC = '\033[0m'

    state_color = {"ERROR": RED, "OK": GREEN, "WARNING": YELLOW}

    @classmethod
    def color(cls, text):
        return cls.state_color[text] + text + cls.ENDC if text in cls.state_color.keys() else text
