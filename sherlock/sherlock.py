import glob
import sys
import os

from prettytable import PrettyTable
from tango import Database, DevFailed

from sherlock.utils.colors import ColoredText
from sherlock.utils.device import Device, Subdevice, ControllerFile

properties = ["ctrl_id", "elements", "motor_role_ids", "source", "devicename",
              "eigerdevice", "adlinkaidevicename", "femtopandads"]

sardana_controllers = ["DiscretePseudoMotorController.py", "DummyCounterTimerController.py", "DummyIORController.py",
                       "DummyMotorController.py", "DummyOneDController.py", "DummyTriggerGateController.py",
                       "DummyTwoDController.py", "DummyZeroDController.py", "HklPseudoMotorController.py", "IoverI0.py",
                       "Slit.py", "TaurusController.py"]


class Sherlock:
    def __init__(self, pool):
        self.devices_in_pool = []
        self.files_in_pool = []

        self.db = Database()

        self.pool_name = f"Pool/{pool}"
        self.pool_device = None

    def start(self):
        self.get_devices_from_pool()
        self.get_files_from_pool()

        for device in self.devices_in_pool:
            self.check_subdevices(device)
            self.check_file(device)

        self.print_report()

    def get_devices_from_pool(self):

        devices_names = self.db.get_device_class_list(self.pool_name)
        devices = list(zip(devices_names[::2], devices_names[1::2]))  # made tuple (device_name, device_class)

        self.pool_device = self._pop_device(devices, "Pool")
        self._pop_device(devices, "DServer")

        for device in devices:
            device_id = self.db.get_device_property(device[0], "id")["id"]
            device_alias = self.get_alias(device[0])

            if device_id:
                device_id = int(device_id[0])
            else:
                print(f"{ColoredText.color('WARNING')}! Device {device} has no id")

            subdevices = self.db.get_device_property(device[0], properties)
            subdevices = [Subdevice(item, self.get_alias(item)) for sublist in [*subdevices.values()] for item in
                          sublist]
            # made flat list with all subdevices

            file = self.db.get_device_property(device[0], "library")["library"]
            file = ControllerFile(file)

            device_instance = Device(device_id, device[0], device_alias, device[1], subdevices, file)

            self.devices_in_pool.append(device_instance)

    def _pop_device(self, devices, filtered_class):
        device = list(filter(lambda x: x[1] == filtered_class, devices))[0]
        devices.remove(device)
        return device

    def get_alias(self, device):
        try:
            return self.db.get_alias_from_device(device)
        except DevFailed:
            return ""

    def get_files_from_pool(self):
        pool_paths = self.db.get_device_property(self.pool_device[0], "PoolPath")["PoolPath"]

        for path in pool_paths:
            if not os.access(path, os.R_OK):
                print(f"{ColoredText.color('WARNING')}! You don't have access to path {path}")
            self.files_in_pool += [path.rsplit("/", 1)[-1] for path in glob.glob(f"{path}/*.py")]

    def check_file(self, device):
        if device.file.name:

            if device.file.name in self.files_in_pool or device.file.name in sardana_controllers:
                device.file.error = False
            else:
                device.file.error = True
                device.file.desc = "Missing library. Check PoolPath!"

    def check_subdevices(self, device):
        for device in device.subdevices:
            self._check_device(device)

    def _check_device(self, device):
        if device.id_.isdigit():
            ids = [device.id_ for device in self.devices_in_pool]
            device.error = int(device.id_) not in ids
            device.desc = f"Device with ID {device.id_} not defined in the Pool!"
        else:
            if device.id_.count("/") == 3:
                device.id_ = device.id_.rsplit("/", 1)[0]
                # e.g sys/tg_test/1/long_scalar
            if "tango://" in device.id_:
                if device.id_.count("/") == 6:
                    device.id_ = device.id_.split("/", 3)[-1].rsplit("/", 1)[0]
                    # e.g. tango://b-v-finest-csdb-0.maxiv.lu.se:10000/B112A-OB04/OPT/SLIT-01-SSE/gapsize
                if device.id_.count("/") == 5:
                    device.id_ = device.id_.split("/", 3)[-1]
                    # e.g. tango://b-v-finest-csdb-0.maxiv.lu.se:10000/B112A-OB04/OPT/SLIT-01-SSE

            try:
                self.db.get_device_info(device.id_)
                device.error = False
            except DevFailed:
                device.error = True
                device.desc = f"Device {device.id_} not defined in the database!"

    def print_report(self):
        devices = PrettyTable()
        subdevices = PrettyTable()
        files = PrettyTable()

        devices.field_names = ["Name", "Alias", "Class", "Status", "Description"]
        devices.title = "\n Devices in Pool"

        subdevices.field_names = ["Parent device", "Subdevice", "Alias", "Status", "Description"]
        subdevices.title = "\n Subdevices with error"

        files.field_names = ["Parent device", "Alias", "File", "Status", "Description"]
        files.title = "\n Missing libraries"

        for device in self.devices_in_pool:
            subdevices_with_error = list(filter(lambda x: x.error, device.subdevices))

            status = "OK" if not subdevices_with_error and not device.file.error else "ERROR"
            desc = "Subdevice error" if subdevices_with_error else ""
            if device.file.error:
                desc = "Missing library"

            devices.add_row([device.name, device.alias, device.class_, ColoredText.color(status), desc])

            for element in subdevices_with_error:
                subdevices.add_row(
                    [device.name, element.id_, element.alias, ColoredText.color("ERROR"), element.desc])

            if device.file.error:
                files.add_row(
                    [device.name, device.alias, device.file.name, ColoredText.color("ERROR"), device.file.desc])

        print(devices.title)
        print(devices.get_string())

        if subdevices._rows:
            print(subdevices.title)
            print(subdevices.get_string())

        if files._rows:
            print(files.title)
            print(files.get_string())


def main():
    try:
        pool = str(sys.argv[1])

        sh = Sherlock(pool)
        sh.start()
    except IndexError:
        print("Entry pool name!")


if __name__ == '__main__':
    main()
