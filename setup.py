#!/usr/bin/env python

###############################################################################
#     sardana-sherlock
#
#     Copyright (C) 2021  MAX IV Laboratory, Lund Sweden.
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see [http://www.gnu.org/licenses/].
###############################################################################

from setuptools import setup, find_packages


def main():
    """Main method collecting all the parameters to setup."""
    name = "sherlock"
    version = "1.0.2"
    description = "Script to check if Sardana config is ok."
    author = "KITS"
    author_email = "kits-sw@maxiv.lu.se"
    license = "GPLv3"
    url = "https://gitlab.maxiv.lu.se/kits-maxiv/app-maxiv-sherlock"
    packages = find_packages()
    entry_points = {'console_scripts': ["sherlock=sherlock.sherlock:main"]
                    }

    python_requires = ">=3.6"
    setup_requires = ["pytest-runner", "setuptools"]
    install_requires = ["prettytable", "pytango"]
    tests_require = ["pytest", "pytest-mock", "mock"]

    setup(
        name=name,
        version=version,
        description=description,
        author=author,
        author_email=author_email,
        license=license,
        url=url,
        entry_points=entry_points,
        packages=packages,
        python_requires=python_requires,
        setup_requires=setup_requires,
        install_requires=install_requires,
        tests_require=tests_require
    )


if __name__ == "__main__":
    main()
