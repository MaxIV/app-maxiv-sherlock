# app-maxiv-sherlock

Script used for checking if Pool config is correct and all element exist.

### Example of usage
`Sherlock name_of_pool`

### Example of output

```

 Devices in Pool
+---------------------------------------+--------+------------------+--------+-----------------+
|                  Name                 | Alias  |      Class       | Status |   Description   |
+---------------------------------------+--------+------------------+--------+-----------------+
| controller/dummymotorcontroller/motor | motor  |    Controller    |   OK   |                 |
|   controller/lumostestcontroller/tc   |   tc   |    Controller    | ERROR  | Missing library |
|              expchan/tc/1             |  tc1   |  TwoDExpChannel  |   OK   |                 |
|              expchan/tc/2             |  tc2   |  TwoDExpChannel  |   OK   |                 |
|   mntgrp/pool_lib_maxiv_lumos_1/mg1   |  mg1   | MeasurementGroup | ERROR  | Subdevice error |
|             motor/motor/1             | motor1 |      Motor       |   OK   |                 |
+---------------------------------------+--------+------------------+--------+-----------------+

 Subdevices with error
+-----------------------------------+-----------+-------+--------+------------------------------------------------+
|           Parent device           | Subdevice | Alias | Status |                  Description                   |
+-----------------------------------+-----------+-------+--------+------------------------------------------------+
| mntgrp/pool_lib_maxiv_lumos_1/mg1 |   121212  |       | ERROR  | Device with ID 121212 not defined in the Pool! |
+-----------------------------------+-----------+-------+--------+------------------------------------------------+

 Missing libraries
+-----------------------------------+-------+-------------------------+--------+----------------------------------+
|           Parent device           | Alias |           File          | Status |           Description            |
+-----------------------------------+-------+-------------------------+--------+----------------------------------+
| controller/lumostestcontroller/tc |   tc  | LumosTestController2.py | ERROR  | Missing library. Check PoolPath! |
+-----------------------------------+-------+-------------------------+--------+----------------------------------+

```
